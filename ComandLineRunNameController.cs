﻿using CommandLineRunModule.Services;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CommandLineRunModule
{
    public class ComandLineRunNameController : INameTransform
    {
        public Globals Globals { get; private set; }

        public bool IsReferenceCompatible => false;

        public bool IsAspNetProject { get; set; }

        public bool IsResponsible(string idClass)
        {
            return idClass == Config.LocalData.Class_Comand_Line.GUID || idClass == Config.LocalData.Class_Comand_Line__Run_.GUID;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> TransformNames(List<clsOntologyItem> items, bool encodeName = true, string idClass = null, IMessageOutput messageOutput = null)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(async() =>
           {
               var result = new ResultItem<List<clsOntologyItem>>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = items
               };

               var elasticAgent = new ServiceAgentElastic(Globals);

               messageOutput?.OutputInfo("Getting the configuration...");

               var modelResult = await elasticAgent.GetTransformNamesModel();

               result.ResultState = modelResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   messageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               messageOutput?.OutputInfo("Have the configuration.");

               var template = modelResult.Result.TransformName?.Val_String ?? "";

               var comandLineRunController = new CommandLineRunController(Globals);

               messageOutput?.OutputInfo("Get items to transform...");
               var itemsToTransform = items.Where(item => item.GUID_Parent == Config.LocalData.Class_Comand_Line.GUID || item.GUID_Parent == Config.LocalData.Class_Comand_Line__Run_.GUID).ToList();
               messageOutput?.OutputInfo($"Have {itemsToTransform.Count}");

               foreach (var itemToTransform in itemsToTransform)
               {
                   var comandLineRunResult = await comandLineRunController.GetCommandLineRun(itemToTransform);

                   result.ResultState = comandLineRunResult.ResultState;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Command Line Run!";
                       messageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }

                   var content = HttpUtility.HtmlEncode(comandLineRunResult.Result.CodeParsed);
                   if (!string.IsNullOrEmpty(template))
                   {
                       content = template.Replace("@CONTENT@", content);
                       
                   }

                   if (encodeName)
                   {
                       itemToTransform.Val_String = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(content));
                   }
                   else
                   {
                       itemToTransform.Name = content;
                   }
                   
               }

               return result;
           });

            return taskResult;
        }

        public ComandLineRunNameController(Globals globals)
        {
            Globals = globals;
        }
    }
}
