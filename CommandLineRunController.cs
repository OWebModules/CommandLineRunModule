﻿using CommandLineRunModule.Factories;
using CommandLineRunModule.Models;
using CommandLineRunModule.Services;
using CommandLineRunModule.Validation;
using ImportExport_Module;
using ImportExport_Module.Models;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using ReportModule.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CommandLineRunModule
{
    public class CommandLineRunController : AppController
    {

        public clsOntologyItem ClassComandLineRun => Config.LocalData.Class_Comand_Line__Run_;

        public async Task<ResultItem<CodeSnipplet>> GetCodeSnipplet(clsOntologyItem refItem)
        {
            var serviceAgent = new ServiceAgentElastic(Globals);
            var resultService = await serviceAgent.GetCodeSnipplets(refItem);
            var result = new ResultItem<CodeSnipplet>
            {
                ResultState = Globals.LState_Success.Clone()
            };

            if (resultService.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultService.ResultState;
                return result;
            }

            var factory = new CodeSnippletsFactories();
            var resultFactory = await factory.CreateCodeSnippletList(resultService.Result, Globals);

            if (resultFactory.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultFactory.ResultState;
                return result;
            }

            result.Result = resultFactory.Result.FirstOrDefault();

            if (result.Result == null)
            {
                result.Result = new CodeSnipplet
                {
                    IsNew = true
                };
            }

            return result;
        }

        public async Task<ResultItem<List<CommandLineRunToReport>>> GetCommandLineRunsOfReports(GetCommandRunsToReportRequest request, bool isAspNetProject)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<List<CommandLineRunToReport>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<CommandLineRunToReport>()
                };

                request.MessageOutput?.OutputInfo("Validate request...");

                result.ResultState = ValidationController.ValidateGetCommandRunsToReportRequest(request, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Validated request.");

                var reportController = new ReportModule.ReportController(Globals, isAspNetProject);

                var reportItemResult = await reportController.GetObject(request.ReportItem.GUID);
                result.ResultState = reportItemResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the report!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                if (reportItemResult.Result.GUID_Parent != reportController.ClassReport.GUID)
                {
                    result.ResultState = Globals.LState_Nothing.Clone();
                    return result;
                }

                request.MessageOutput?.OutputInfo("Get Report...");

                var reportResult = await reportController.GetReport(reportItemResult.Result);

                result.ResultState = reportResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var report = reportResult.Result.Report;
                var reportFields = reportResult.Result.ReportFields;

                request.MessageOutput?.OutputInfo("Have Report and Report-Fields.");


                request.MessageOutput?.OutputInfo("Get CommandLinerun to Report model...");
                var elasticAgent = new ServiceAgentElastic(Globals);
                var getModelResult = await elasticAgent.GetCommandLineRunToReport(request.ReportItem);

                result.ResultState = getModelResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have CommandLineRun to Report model.");

                result.Result = (from cmdlrToRep in getModelResult.Result.ReportItemToCommandLineRunToReport
                                 join cmdlrToRepToCmd in getModelResult.Result.CmdrlToRepToCommandLineRun on cmdlrToRep.ID_Object equals cmdlrToRepToCmd.ID_Object
                                 select new CommandLineRunToReport
                                 {
                                     IdCommandLineToReport = cmdlrToRep.ID_Object,
                                     NameCommandLineToReport = cmdlrToRep.Name_Object,
                                     IdCommandLineRun = cmdlrToRepToCmd.ID_Other,
                                     NameCommandLineRun = cmdlrToRepToCmd.Name_Other,
                                     IdReport = cmdlrToRep.ID_Other,
                                     NameReport = cmdlrToRep.ID_Other,
                                     VariablesToFields = getModelResult.Result.CmdrlToRepToVariablesToFields.Where(rel1 => rel1.ID_Other == cmdlrToRep.ID_Object).Select(rel1 => new Models.VariableToField
                                     {
                                         IdVariableToField = rel1.ID_Object,
                                         NameVariableToField = rel1.Name_Object
                                     }).ToList()
                                 }).ToList();

                foreach (var varToField in result.Result.SelectMany(repToCommandLRun => repToCommandLRun.VariablesToFields).ToList())
                {
                    varToField.ReportField = (from varToFieldToRepField in getModelResult.Result.VarToFieldToReportFields.Where(varField => varField.ID_Object == varToField.IdVariableToField)
                                              join reportField in reportFields on varToFieldToRepField.ID_Other equals reportField.IdReportField
                                              select reportField).FirstOrDefault();
                    varToField.Variable = (getModelResult.Result.VarToFieldToVariables.Where(varField => varField.ID_Object == varToField.IdVariableToField)
                                           .Select(varToVar => new Variable
                                           {
                                               IdVariable = varToVar.ID_Other,
                                               NameVariable = varToVar.Name_Other
                                           })).FirstOrDefault();
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<CommandLineRun>> GetCommandLineRun(clsOntologyItem commandLineRun, List<CommandLineRunToReport> commandLineRunToReports = null, ReportDataItem reportData = null)
        {
            var serviceAgent = new ServiceAgentElastic(Globals);
            var resultServices = await serviceAgent.GetCommandLineRun(new List<clsOntologyItem> { commandLineRun });

            var result = new ResultItem<CommandLineRun>
            {
                ResultState = resultServices.ResultState
            };

            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                return result;
            }

            var factory = new CommandLineRunFactory();

            var resultFactory = await factory.CreateCommandLineRun(commandLineRun, resultServices.Result, commandLineRunToReports, Globals, reportData);

            if (resultFactory.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultFactory.ResultState;
                return result;
            }

            result.Result = resultFactory.Result;

            return result;
        }

        public async Task<ResultItem<CreateVarValuesResult>> CreateVarValues(CreateVarValuesRequest request)
        {
            var taskResult = await Task.Run<ResultItem<CreateVarValuesResult>>(async () =>
            {
                var result = new ResultItem<CreateVarValuesResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new CreateVarValuesResult()
                };

                var elasticAgent = new ServiceAgentElastic(Globals);
                request.MessageOutput?.OutputInfo($"Get CommandLineRun OItem");
                var oItemResult = await elasticAgent.GetOItem(request.IdCommandlineRun);

                result.ResultState = oItemResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo($"Have CommandLineRun OItem: {oItemResult.Result.ObjectItem.Name}");

                request.MessageOutput?.OutputInfo($"Get CommandLineRun Item");
                var commandLineResult = await GetCommandLineRun(oItemResult.Result.ObjectItem);

                result.ResultState = commandLineResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo($"Have CommandLineRun Item: {commandLineResult.Result.NameRoot_CommandLineRun}\\{commandLineResult.Result.Name_CommandLineRun}");

                var excelImporter = new ExcelImporter(Globals);

                var excelImportRequest = new CreateExcelDataTableRequest
                {
                    FilePathExcel = request.ExcelFilePath,
                    Sheets = new List<string>
                    {
                        request.SheetName
                    }
                };

                request.MessageOutput?.OutputInfo($"Create Datatable from Excel-File: {request.ExcelFilePath}");
                var dataTableResult = await excelImporter.CreateExcelDataTable(excelImportRequest);
                result.ResultState = dataTableResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var dataTable = dataTableResult.ExcelDataTables.FirstOrDefault(tabl => tabl.Sheet == request.SheetName);
                if (dataTable == null)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"The Sheet {request.SheetName} cannot be found!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo($"Created Datatable: {dataTable.Sheet}, Count of Rows: {dataTable.DataTable.Rows.Count}");

                var skipLine = false;
                if (request.RemoveHeader)
                {
                    skipLine = true;
                }
                long rowId = 1;
                List<VarValue> varValues = new List<VarValue>();

                foreach (DataRow item in dataTable.DataTable.Rows)
                {
                    if (skipLine == true)
                    {
                        skipLine = false;
                        rowId++;
                        continue;
                    }

                    var varValue = new VarValue
                    {
                        Variable = item[(int)request.ColIndexVariables].ToString(),
                        Value = item[(int)request.ColIndexValues].ToString()
                    };

                    if (string.IsNullOrEmpty(varValue.Variable)) continue;

                    varValues.Add(varValue);

                    if (!request.NoImport)
                    {

                        varValue.VariableItem = commandLineResult.Result.Variables.FirstOrDefault(var => var.Name == varValue.Variable);

                        if (varValue.VariableItem != null)
                        {
                            varValue.ValueItem = (from valVar in commandLineResult.Result.ValuesToVariables.Where(valVar => valVar.ID_Other == varValue.VariableItem.GUID)
                                                  join valRef in commandLineResult.Result.ValuesToRef on valVar.ID_Object equals valRef.ID_Object into valRefs
                                                  from valRef in valRefs.DefaultIfEmpty()
                                                  where valRef != null ? valRef.Name_Other == varValue.Value : valVar.Name_Object == varValue.Value
                                                  select new clsOntologyItem
                                                  {
                                                      GUID = valVar.ID_Object,
                                                      Name = valVar.Name_Object,
                                                      GUID_Parent = valVar.ID_Parent_Object,
                                                      Type = Globals.Type_Object
                                                  }).FirstOrDefault();

                        }

                        var message = $"Row {rowId}: ";
                        if (varValue.VariableItem != null)
                        {
                            message = $"{message} Found Variable {varValue.Variable}";
                        }
                        else
                        {
                            message = $"{message} No Variable Found {varValue.Variable}";
                        }

                        if (varValue.ValueItem != null)
                        {
                            message = $"{message} | Found Value {varValue.Value}";
                        }
                        else
                        {
                            message = $"{message} | No value Found";
                        }

                        request.MessageOutput?.OutputInfo(message);

                        if (varValue.VariableItem == null)
                        {
                            varValue.VariableItem = new clsOntologyItem
                            {
                                Name = varValue.Variable,
                                GUID_Parent = Config.LocalData.Class_Variable.GUID,
                                New_Item = true,
                                Type = Globals.Type_Object
                            };
                            varValue.ValueItem = new clsOntologyItem
                            {
                                Name = varValue.Value,
                                GUID_Parent = Config.LocalData.Class_Value.GUID,
                                New_Item = true,
                                Type = Globals.Type_Object
                            };
                        }

                        if (varValue.ValueItem == null)
                        {
                            varValue.ValueItem = new clsOntologyItem
                            {
                                Name = varValue.Value,
                                GUID_Parent = Config.LocalData.Class_Value.GUID,
                                New_Item = true,
                                Type = Globals.Type_Object
                            };
                        }
                    }

                    rowId++;
                }

                if (request.NoImport && !string.IsNullOrEmpty(request.ExportPath))
                {
                    var codeParsed = commandLineResult.Result.Code;
                    foreach (var varValue in varValues)
                    {
                        codeParsed = codeParsed.Replace($"@{varValue.Variable}@", varValue.Value);
                        request.MessageOutput?.OutputInfo($"Variable: {varValue.Variable} / {varValue.Value}");
                    }
                    try
                    {
                        System.IO.File.WriteAllText(request.ExportPath, codeParsed, Encoding.UTF8);
                    }
                    catch (Exception ex)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"Error while saveing the output-file {request.ExportPath}";
                    }
                }
                else
                {
                    var serviceAgent = new ServiceAgentElastic(Globals);
                    var nonExistingVariables = varValues.Where(var => !string.IsNullOrEmpty(var.Variable) && var.VariableItem.New_Item != null && var.VariableItem.New_Item.Value).Select(var => var.VariableItem).ToList();
                    var nonExistingValues = varValues.Where(var => var.ValueItem != null && !string.IsNullOrEmpty(var.Value) && var.ValueItem.New_Item != null && var.ValueItem.New_Item.Value).Select(val => val.ValueItem).ToList();
                    var variableItems = await serviceAgent.CheckObjects(nonExistingVariables);
                    var valsToRefs = serviceAgent.GetValuesToReferences();

                    result.ResultState = valsToRefs.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    var newItems = (from nonExisting in nonExistingValues
                                    join rel in valsToRefs.Result on nonExisting.GUID equals rel.ID_Object into rels
                                    from rel in rels.DefaultIfEmpty()
                                    where rel == null
                                    select nonExisting).ToList();

                    var saveResult = await serviceAgent.SaveObjects(newItems);

                    result.ResultState = saveResult.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    var checkItems = (from nonExisting in nonExistingValues
                                      join newItem in newItems on nonExisting equals newItem into newItems1
                                      from newItem in newItems1.DefaultIfEmpty()
                                      where newItem == null
                                      select nonExisting).ToList();

                    var checkResult = await serviceAgent.CheckObjects(checkItems);
                    result.ResultState = checkResult.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }
                    var valueItems = saveResult.Result;
                    valueItems.AddRange(checkResult.Result);

                    //var varItems = varValues.Where(var => var.VariableItem.New_Item == null || !var.VariableItem.New_Item.Value).Select(var => var.VariableItem).ToList();
                    //varItems.AddRange(variableItems.Result);



                    var relationItems = new List<clsObjectRel>();
                    foreach (var varValue in varValues)
                    {

                        if (varValue.VariableItem == null || varValue.ValueItem == null)
                        {
                            continue;
                        }
                        var newVar = false;
                        var newVal = false;
                        if (varValue.VariableItem.New_Item != null && varValue.VariableItem.New_Item.Value)
                        {
                            newVar = true;
                            varValue.VariableItem = variableItems.Result.First(var => var.Name == varValue.Variable);
                        }

                        if (varValue.ValueItem.New_Item != null && varValue.ValueItem.New_Item.Value)
                        {
                            newVal = true;
                            varValue.ValueItem = valueItems.First(var => var.Name == varValue.Value);
                        }

                        if (newVar || newVal)
                        {
                            var relsResult = serviceAgent.GetRelationVarValueCmdrl(new clsOntologyItem
                            {
                                GUID = commandLineResult.Result.ID_CommandLineRun,
                                Name = commandLineResult.Result.Name_CommandLineRun,
                                GUID_Parent = Config.LocalData.Class_Comand_Line__Run_.GUID,
                                Type = Globals.Type_Object
                            }, varValue.VariableItem, varValue.ValueItem);

                            result.ResultState = relsResult.ResultState;

                            if (result.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                return result;
                            }

                            relationItems.AddRange(relsResult.Result);
                        }
                    }

                    relationItems = relationItems.Where(relItem => relItem != null).ToList();
                    if (relationItems.Any())
                    {
                        var saveRelResult = await elasticAgent.SaveRelations(relationItems);

                        result.ResultState = saveRelResult;

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            return result;
                        }
                    }
                }

                result.Result.ValueCount = varValues.Count;
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<AddVariablesResult>> AddVariables(AddVariablesRequest request)
        {
            var taskResult = await Task.Run<ResultItem<AddVariablesResult>>(async () =>
           {
               var result = new ResultItem<AddVariablesResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new AddVariablesResult()
               };

               var serviceAgent = new ServiceAgentElastic(Globals);

               request.MessageOutput?.OutputInfo("Get CommandLine or CodeSnipplet...");
               var resultCommandLineOrCodeSnipplet = await serviceAgent.GetCommandLineOrCodeSnipplet(request.IdCodeSnippletOrCommandLine);


               result.ResultState = resultCommandLineOrCodeSnipplet.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }
               request.MessageOutput?.OutputInfo($"Have CommandLine or CodeSnipplet: {resultCommandLineOrCodeSnipplet.Result.CommandLineOrCodeSnipplet.Name}");

               if (resultCommandLineOrCodeSnipplet.Result.CommandLineOrCodeSnipplet == null)
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No Command Line or Code Snipplet found!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               if (resultCommandLineOrCodeSnipplet.Result.CommandLineOrCodeSnipplet.GUID_Parent != Config.LocalData.Class_Comand_Line.GUID &&
                    resultCommandLineOrCodeSnipplet.Result.CommandLineOrCodeSnipplet.GUID_Parent != Config.LocalData.Class_Code_Snipplets.GUID)
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "The requested id don't represent a Command Line or Code Snipplet!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               if (resultCommandLineOrCodeSnipplet.Result.CommandLineOrCodeSnipplet.GUID_Parent == Config.LocalData.Class_Code_Snipplets.GUID &&
                    resultCommandLineOrCodeSnipplet.Result.Code == null)
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No code of Code Snipplet found!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo($"Get Variables...");
               var variablesResult = serviceAgent.GetCommandLineOrCodeSnippletsToVariables(request.IdCodeSnippletOrCommandLine);

               result.ResultState = variablesResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the variables!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo($"Have Variables: {variablesResult.Result.Count} (Count)");

               var regexVariables = new Regex(@"@\S+@");

               request.MessageOutput?.OutputInfo($"Search Variables...");
               var matches = regexVariables.Matches(resultCommandLineOrCodeSnipplet.Result.CommandLineOrCodeSnipplet.GUID_Parent == Config.LocalData.Class_Comand_Line.GUID ?
                   resultCommandLineOrCodeSnipplet.Result.CommandLineOrCodeSnipplet.Name : resultCommandLineOrCodeSnipplet.Result.Code.Val_String);
               request.MessageOutput?.OutputInfo($"Found Variables: {matches.Count} (Count)");


               var searchVariables = new List<clsOntologyItem>();
               foreach (Match match in matches)
               {
                   request.MessageOutput?.OutputInfo($"Found Variable: {match.Value}");
                   var variable = match.Value.Replace("@", "");
                   var existingVariable = variablesResult.Result.FirstOrDefault(var => var.Name_Other == variable);

                   if (existingVariable == null && !searchVariables.Any(var => var.Name == variable))
                   {
                       searchVariables.Add(new clsOntologyItem
                       {
                           Name = variable,
                           GUID_Parent = Config.LocalData.Class_Variable.GUID,
                           Type = Globals.Type_Object
                       });
                   }

               }

               request.MessageOutput?.OutputInfo($"Check Variables: {searchVariables.Count}");
               if (searchVariables.Any())
               {
                   var variablesCheckResult = await serviceAgent.CheckObjects(searchVariables);
                   result.ResultState = variablesCheckResult.ResultState;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }

                   var saveResult = await serviceAgent.SaveCommandLineOrCodeSnippletToVariable(resultCommandLineOrCodeSnipplet.Result.CommandLineOrCodeSnipplet, variablesCheckResult.Result);

                   result.ResultState = saveResult;
                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }
               }

               result.Result.Variables = searchVariables;
               request.MessageOutput?.OutputInfo($"Related Variable: {searchVariables.Count}");

               return result;
           });

            return taskResult;
        }

        public CommandLineRunController(Globals globals) : base(globals)
        {
        }

    }
}
