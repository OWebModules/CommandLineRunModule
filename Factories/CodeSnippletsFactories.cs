﻿using CommandLineRunModule.Models;
using CommandLineRunModule.Services;
using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRunModule.Factories
{
    public class CodeSnippletsFactories
    {
        public async Task<ResultItem<List<CodeSnipplet>>> CreateCodeSnippletList(CodeSnippletRaw codeSnippletRaw, Globals globals)
        {
            var taskResult = await Task.Run<ResultItem<List<CodeSnipplet>>>(() =>
            {
                var result = new ResultItem<List<CodeSnipplet>>()
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var syntaxHighlightingToProgramingLanguages = (from programingLanguage in codeSnippletRaw.CodeSnippletToProgramingLanguage
                                                               join syntaxHighlight in codeSnippletRaw.ProgramingLanguageToSyntaxHighlighting on programingLanguage.ID_Other equals syntaxHighlight.ID_Other 
                                                               select new { programingLanguage, syntaxHighlight });


                result.Result = (from codeSnipplet in codeSnippletRaw.CodeSnipplets
                                 join code in codeSnippletRaw.CodeOfCodeSnipplets on codeSnipplet.GUID equals code.ID_Object
                                 join syntaxHighlightingToProgramingLanguage in syntaxHighlightingToProgramingLanguages on codeSnipplet.GUID equals syntaxHighlightingToProgramingLanguage.programingLanguage.ID_Object into syntaxHighlightsAndProgramingLanguages
                                 from syntaxHighlightingToProgramingLanguage in syntaxHighlightsAndProgramingLanguages.DefaultIfEmpty()
                                 select new CodeSnipplet
                                 {
                                     IdCodeSnipplet = codeSnipplet.GUID,
                                     NameCodeSnipplet = codeSnipplet.Name,
                                     IdAttributeCode = code.ID_Attribute,
                                     Code = code.Val_String,
                                     IdProgrammingLanguage = syntaxHighlightingToProgramingLanguage != null ? syntaxHighlightingToProgramingLanguage.programingLanguage.ID_Other : null,
                                     NameProgrammingLanguage = syntaxHighlightingToProgramingLanguage != null ? syntaxHighlightingToProgramingLanguage.programingLanguage.Name_Other : null,
                                     IdSyntaxHighlight = syntaxHighlightingToProgramingLanguage != null ? syntaxHighlightingToProgramingLanguage.syntaxHighlight.ID_Other : null,
                                     NameSyntaxHighlight = syntaxHighlightingToProgramingLanguage != null ? syntaxHighlightingToProgramingLanguage.syntaxHighlight.Name_Other : null,
                                     Variables = codeSnippletRaw.CodeSnippletToVariables.Where(codeToVar => codeToVar.ID_Object == codeSnipplet.GUID).Select(codeToVar => new Variable { IdVariable = codeToVar.ID_Other, NameVariable = codeToVar.Name_Other }).ToList()
                                 }).ToList();

                return result;
            });

            return taskResult;
        }
    }
}
