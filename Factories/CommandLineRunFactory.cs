﻿using CommandLineRunModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using ReportModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CommandLineRunModule.Factories
{
    public class CommandLineRunFactory
    {
        private ResultItem<List<clsOntologyItem>> GetSubCommandLineRuns(clsOntologyItem commandLineRun, List<clsObjectRel> relation, Globals globals)
        {
            var result = new ResultItem<List<clsOntologyItem>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsOntologyItem>()
            };

            result.Result = relation.Where(rel => rel.ID_Object == commandLineRun.GUID).OrderBy(rel => rel.OrderID.Value).Select(rel => new clsOntologyItem
            {
                GUID = rel.ID_Other,
                Name = rel.Name_Other,
                GUID_Parent = rel.ID_Parent_Other,
                Type = rel.Ontology
            }).ToList();

            var additional = new List<clsOntologyItem>();

            foreach (var commandLineRunSub in result.Result)
            {
                var resultSubItems = GetSubCommandLineRuns(commandLineRunSub, relation, globals);
                if (resultSubItems.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = resultSubItems.ResultState;
                    return result;
                }
                additional.AddRange(resultSubItems.Result);
            }

            result.Result.AddRange(additional);

            return result;
        }

        public async Task<ResultItem<CommandLineRun>> CreateCommandLineRun(clsOntologyItem commandLineRun, CommandLineRunRaw commandLineRunRaw, List<CommandLineRunToReport> commandLineRunToReports, Globals globals, ReportDataItem reportData = null)
        {
            var taskResult = await Task.Run<ResultItem<CommandLineRun>>(async () =>
            {
                var result = new ResultItem<CommandLineRun>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new CommandLineRun()
                };

                var subCommandLineRunsResult = GetSubCommandLineRuns(commandLineRun, commandLineRunRaw.CommandLineRunsHierarchy, globals);
                if (subCommandLineRunsResult.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = subCommandLineRunsResult.ResultState;
                    return result;
                }

                var commandLineRuns = subCommandLineRunsResult.Result;
                commandLineRuns.Insert(0, commandLineRun);

                var programmingLanguages = new List<clsObjectRel>();

                var codeSnippletsOrCommands = (from commanlineRun in commandLineRuns
                                               join commandToCodeSnipplet in commandLineRunRaw.CommandLineRunsToCodeSnipplets on commanlineRun.GUID equals commandToCodeSnipplet.ID_Object
                                               select commandToCodeSnipplet).ToList();


                codeSnippletsOrCommands.AddRange((from commanlineRun in commandLineRuns
                                                  join commandToCommandLine in commandLineRunRaw.CommandLineRunsToCommandLines on commanlineRun.GUID equals commandToCommandLine.ID_Object
                                                  select commandToCommandLine));


                var codeSnippOrCmdToProgrammingLanguages = commandLineRunRaw.CodeSnippletsToProgrammingLanguages;
                codeSnippOrCmdToProgrammingLanguages.AddRange(commandLineRunRaw.CommandLinesToProgrammingLanguage);

                programmingLanguages = (from codeSnippletOrCommand in codeSnippletsOrCommands
                                        join programmingLanguage in codeSnippOrCmdToProgrammingLanguages on codeSnippletOrCommand.ID_Other equals programmingLanguage.ID_Object
                                        select programmingLanguage).ToList();

                var programmingLanguageMost = programmingLanguages.GroupBy(prog => new { prog.ID_Other, prog.Name_Other }).Select(prog => new { ID_ProgrammingLanguage = prog.Key.ID_Other, Name_ProgrammingLanguage = prog.Key.Name_Other, Count = prog.Count() }).
                    OrderByDescending(prog => prog.Count).FirstOrDefault();

                var encoding = commandLineRunRaw.ProgramingLanguageToEncoding.FirstOrDefault(progToEnc => progToEnc.ID_Object == programmingLanguageMost.ID_ProgrammingLanguage);

                var syntaxHighlight = commandLineRunRaw.ProgramingLanguageToSyntaxHighlighting.FirstOrDefault(progToSyntax => progToSyntax.ID_Object == programmingLanguageMost.ID_ProgrammingLanguage);

                var references = commandLineRunRaw.CommandLineRunsToReferences.Where(rel => rel.ID_Object == commandLineRun.GUID).ToList();

                result.Result = new CommandLineRun
                {
                    ID_CommandLineRun = commandLineRun.GUID,
                    Name_CommandLineRun = commandLineRun.Name,
                    ID_ProgrammingLanguage = programmingLanguageMost?.ID_ProgrammingLanguage,
                    Name_ProgrammingLanguage = programmingLanguageMost?.Name_ProgrammingLanguage,
                    ID_Encoding = encoding?.ID_Other,
                    Name_Encoding = encoding?.Name_Other,
                    ID_SyntaxHighlighting = syntaxHighlight?.ID_Other,
                    Name_SyntaxHighlighting = syntaxHighlight?.Name_Other,
                    Code = "",
                    CodeParsed = "",
                    CmdrlToReferences = references
                };

                var codeSnipplets = from codeSnipp in commandLineRunRaw.CommandLineRunsToCodeSnipplets
                                    join codeItm in commandLineRunRaw.CodeSnippletsToCode on codeSnipp.ID_Other equals codeItm.ID_Object
                                    select new { codeSnipp, codeItm };

                var codeItems = (from cmdrl in commandLineRuns
                                 join codeSnipp in codeSnipplets on cmdrl.GUID equals codeSnipp.codeSnipp.ID_Object into codeSnipps
                                 from codeSnipp in codeSnipps.DefaultIfEmpty()
                                 join cmdl in commandLineRunRaw.CommandLineRunsToCommandLines on cmdrl.GUID equals cmdl.ID_Object into cmdls
                                 from cmdl in cmdls.DefaultIfEmpty()
                                 where codeSnipp != null || cmdl != null
                                 select new { cmdrl, codeSnipp, cmdl });










                var variableValuesRef = (from cmdrl in commandLineRuns
                                         join cmdrlToValue in commandLineRunRaw.CommandLineRunsToValues on cmdrl.GUID equals cmdrlToValue.ID_Object
                                         join valueToVariable in commandLineRunRaw.ValuesToVariables on cmdrlToValue.ID_Other equals valueToVariable.ID_Object
                                         join variable in commandLineRunRaw.CommandLineAndCodeSnippletsToVariables on valueToVariable.ID_Other equals variable.ID_Other
                                         join value in commandLineRunRaw.CommandLineRunsToValues on valueToVariable.ID_Object equals value.ID_Other
                                         join refItem in commandLineRunRaw.ValuesToReferences on value.ID_Other equals refItem.ID_Object into refItems
                                         from refItem in refItems.DefaultIfEmpty()
                                         select new { cmdrl, cmdrlToValue, valueToVariable, variable, value, refItem });
                var variableValues = (from cmdrl in commandLineRuns
                                      join cmdrlToValue in variableValuesRef on cmdrl.GUID equals cmdrlToValue.cmdrlToValue.ID_Object
                                      join valueToVariable in variableValuesRef on cmdrlToValue.cmdrlToValue.ID_Other equals valueToVariable.valueToVariable.ID_Object
                                      select new
                                      {
                                          IdCmdrl = cmdrl.GUID,
                                          Variable = $"@{valueToVariable.variable.Name_Other}@",
                                          Value = valueToVariable.refItem != null ? valueToVariable.refItem.Name_Other : valueToVariable.value.Name_Other,
                                          IdRefItem = valueToVariable.refItem?.ID_Other,
                                          VariableItem = valueToVariable.variable,
                                          ValueItem = valueToVariable.value
                                      }).GroupBy(varVal => new { IdCmdRl = varVal.IdCmdrl, Variable = varVal.Variable, Value = varVal.Value, IdRefItem = varVal.IdRefItem }).Select(varValGrp => new { IdCmdRl = varValGrp.Key.IdCmdRl, Variable = varValGrp.Key.Variable, Value = varValGrp.Key.Value, IdRefItem = varValGrp.Key.IdRefItem }).ToList();
                var parsedCode = "";
                
                foreach (var codeItem in codeItems)
                {
                    var code = "";
                    if (codeItem.codeSnipp != null)
                    {
                        code = codeItem.codeSnipp.codeItm.Val_String;

                    }
                    else
                    {
                        code = codeItem.cmdl.Name_Other + "\r\n";

                    }
                    result.Result.Code += code;
                    result.Result.Variables = commandLineRunRaw.CommandLineAndCodeSnippletsToVariables.Where(cmdrl => cmdrl.ID_Object == codeItem.cmdl.ID_Other).Select(var => new clsOntologyItem { GUID = var.ID_Other, Name = var.Name_Other, GUID_Parent = var.ID_Parent_Other, Type = globals.Type_Object }).ToList();
                    result.Result.ValuesToVariables = variableValuesRef.Where(var => var.cmdrl.GUID == codeItem.cmdrl.GUID).Select(val => val.valueToVariable).ToList();
                    result.Result.ValuesToRef = variableValuesRef.Where(var => var.cmdrl.GUID == codeItem.cmdrl.GUID && var.refItem != null).Select(val => val.refItem).ToList();

                    foreach (var varVal in variableValues.Where(var => var.IdCmdRl == codeItem.cmdrl.GUID))
                    {
                        var value = varVal.Value;
                        if (!string.IsNullOrEmpty(varVal.IdRefItem))
                        {
                            var commandLineRunVal = commandLineRunRaw.CommandLineRuns.FirstOrDefault(cmdrl => cmdrl.GUID == varVal.IdRefItem);
                            if (commandLineRunVal != null)
                            {
                                var resultCreateCommandLineRun = await CreateCommandLineRun(commandLineRunVal, commandLineRunRaw, commandLineRunToReports, globals);
                                if (resultCreateCommandLineRun.ResultState.GUID == globals.LState_Error.GUID)
                                {
                                    result.ResultState = resultCreateCommandLineRun.ResultState;
                                    return result;
                                }
                                
                                value = resultCreateCommandLineRun.Result.CodeParsed;
                            }

                        }

                        code = code.Replace(varVal.Variable, value);
                    }

                    parsedCode += code;
                }
                
                if (reportData != null && reportData.CommandLineRunsToReport.Any(cmdrl => cmdrl.IdCommandLineRun == commandLineRun.GUID))
                {
                    var cmdlrToRep = reportData.CommandLineRunsToReport.FirstOrDefault(cmdlr => cmdlr.IdCommandLineRun == commandLineRun.GUID);
                    if (cmdlrToRep != null)
                    {
                        foreach (var reportDataItem in reportData.data)
                        {
                            var sbCodeReportData = new StringBuilder(parsedCode);
                            foreach (var varField in cmdlrToRep.VariablesToFields)
                            {
                                if (varField.ReportField != null)
                                {
                                    if (reportDataItem.ContainsKey(varField.ReportField.NameCol))
                                    {
                                        var value = reportDataItem[varField.ReportField.NameCol]?.ToString() ?? "";
                                        sbCodeReportData.Replace($"@{varField.Variable.NameVariable}@", value);
                                    }
                                }
                            }
                            
                            result.Result.CodeParsed += sbCodeReportData.ToString();
                        }
                    }
                }
                else
                {
                    result.Result.CodeParsed += parsedCode;
                }
                

                var resultGetParent = GetParentCommandLineRun(commandLineRun, commandLineRunRaw, globals);

                if (resultGetParent.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = resultGetParent.ResultState;
                    return result;
                }

                result.Result.IDRoot_CommandLineRun = resultGetParent.Result.GUID;
                result.Result.NameRoot_CommandLineRun = resultGetParent.Result.Name;

                return result;
            });

            return taskResult;
        }

        public ResultItem<clsOntologyItem> GetParentCommandLineRun(clsOntologyItem commandLineRun, CommandLineRunRaw raw, Globals globals)
        {
            var result = new ResultItem<clsOntologyItem>
            {
                ResultState = globals.LState_Success.Clone()
            };
            var parentItems = raw.CommandLineRunsParentsHierarchy.Where(rel => rel.ID_Other == commandLineRun.GUID);

            if (parentItems.Any())
            {
                var parentRel = parentItems.FirstOrDefault();
                var parentItem = new clsOntologyItem
                {
                    GUID = parentRel.ID_Object,
                    Name = parentRel.Name_Object,
                    GUID_Parent = parentRel.ID_Parent_Object,
                    Type = globals.Type_Object
                };

                var resultGetParent = GetParentCommandLineRun(parentItem, raw, globals);
                return resultGetParent;
            }
            else
            {
                result.Result = commandLineRun;
                return result;
            }
        }
    }
}
