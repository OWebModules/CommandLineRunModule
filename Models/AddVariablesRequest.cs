﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRunModule.Models
{
    public class AddVariablesRequest
    {
        public string IdCodeSnippletOrCommandLine { get; private set; }

        public IMessageOutput MessageOutput { get; set; }

        public AddVariablesRequest(string idCodeSnippletOrCommandLine)
        {
            IdCodeSnippletOrCommandLine = idCodeSnippletOrCommandLine;
        }
    }
}
