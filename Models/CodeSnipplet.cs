﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRunModule.Models
{
    public class CodeSnipplet
    {
        public string IdCodeSnipplet { get; set; }
        public string NameCodeSnipplet { get; set; }
        public string IdAttributeCode { get; set; }
        public string Code { get; set; }
        public string IdProgrammingLanguage { get; set; }
        public string NameProgrammingLanguage { get; set; }
        public string IdSyntaxHighlight { get; set; }
        public string NameSyntaxHighlight { get; set; }
        public bool IsNew { get; set; }

        public List<Variable> Variables { get; set; } 

        public CodeSnipplet()
        {
            Variables = new List<Variable>();
        }
    }
}
