﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRunModule.Models
{
    public class CommandLineOrCodeSnippletRaw
    {
        public clsOntologyItem CommandLineOrCodeSnipplet { get; set; }
        public clsObjectAtt Code { get; set; }
    }
}
