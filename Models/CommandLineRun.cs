﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRunModule.Models
{
    public class CommandLineRun
    {
        public string IDRoot_CommandLineRun { get; set; }
        public string NameRoot_CommandLineRun { get; set; }
        public string ID_CommandLineRun { get; set; }
        public string Name_CommandLineRun { get; set; }
        public string ID_CodeItem { get; set; }
        public string Name_CodeItem { get; set; }
        public string ID_ProgrammingLanguage { get; set; }
        public string Name_ProgrammingLanguage { get; set; }
        public string ID_SyntaxHighlighting { get; set; }
        public string Name_SyntaxHighlighting { get; set; }
        public string Code { get; set; }
        public string CodeParsed { get; set; }
        public string ID_Ref { get; set; }
        public string Name_Ref { get; set; }
        public string ID_Encoding { get; set; }
        public string Name_Encoding { get; set; }
        public long OrderID { get; set; }

        public List<clsOntologyItem> Variables { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> ValuesToVariables { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ValuesToRef { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> CmdrlToReferences { get; set; } = new List<clsObjectRel>();

    }
}
