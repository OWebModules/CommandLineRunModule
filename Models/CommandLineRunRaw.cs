﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRunModule.Models
{
    public class CommandLineRunRaw
    {
        public List<clsOntologyItem> CommandLineRuns { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> CommandLineRunsHierarchy { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> CommandLineRunsParentsHierarchy { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> CommandLineRunsToCommandLines { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> CommandLinesToProgrammingLanguage { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> CommandLineRunsToCodeSnipplets { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> CodeSnippletsToCode { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> CodeSnippletsToProgrammingLanguages { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ProgramingLanguageToSyntaxHighlighting { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ProgramingLanguageToEncoding { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> CommandLineAndCodeSnippletsToVariables { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> CommandLineRunsToValues { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ValuesToReferences { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ValuesToVariables { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> CommandLineRunsToReferences { get; set; } = new List<clsObjectRel>();
    }
}
