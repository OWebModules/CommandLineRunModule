﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRunModule.Models
{
    public class CommandLineRunToReport
    {
        public string IdCommandLineToReport { get; set; }

        public string NameCommandLineToReport { get; set; }
        public string IdCommandLineRun { get; set; }
        public string NameCommandLineRun { get; set; }
        public string IdReport { get; set; }
        public string NameReport { get; set; }

        public List<RemoveCharacters> RemoveCharacters { get; set; }

        public List<VariableToField> VariablesToFields { get; set; }


    }
}
