﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRunModule.Models
{
    public class CommandLineRunToReportRaw
    {
        public clsOntologyItem ReportItem { get; set; }
        public List<clsObjectRel> ReportItemToCommandLineRunToReport { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> CmdrlToRepToCommandLineRun { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> CmdrlToRepToRemoveCharacter { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> CmdrlToRepToRemoveCharacterAttributes { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> CmdrlToRepToVariablesToFields { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> VarToFieldToFindReplace { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> VarToFieldToReportFields { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> VarToFieldToVariables { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> VarToFieldToRemoveCharacters { get; set; } = new List<clsObjectRel>();
    }
}
