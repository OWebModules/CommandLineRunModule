﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRunModule.Models
{
    public class CreateVarValuesRequest
    {
        public string IdCommandlineRun { get; set; }
        public string ExcelFilePath { get; set; }

        public string SheetName { get; set; }
        public long ColIndexVariables { get; set; }
        public long ColIndexValues { get; set; }
        public bool RemoveHeader { get; set; }
        public bool NoImport { get; set; }
        public string ExportPath { get; set; }
        public IMessageOutput MessageOutput { get; set; }
    }

}
