﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRunModule.Models
{
    public class FieldReplace
    {
        public string IdFieldReplace { get; set; }
        public string NameFieldReplace { get; set; }

        public string IdAttributeTextForReplace { get; set; }
        public string TextForReplace { get; set; }

        public string IdRegularExpression { get; set; }
        public string NameRegularExpression { get; set; }

        public string IdAttributeRegex { get; set; }
        public string Regex { get; set; }


    }
}
