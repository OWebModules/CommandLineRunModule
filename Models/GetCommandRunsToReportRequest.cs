﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRunModule.Models
{
    public class GetCommandRunsToReportRequest
    {
        public clsOntologyItem ReportItem { get; private set; }
        public IMessageOutput MessageOutput { get; set; }

        public GetCommandRunsToReportRequest(clsOntologyItem reportItem)
        {
            ReportItem = reportItem;
        }
    }
}
