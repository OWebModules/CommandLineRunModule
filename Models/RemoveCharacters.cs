﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRunModule.Models
{
    public class RemoveCharacters
    {
        public string IdRemoveCharacters { get; set; }
        public string NameRemoveCharacters { get; set; }

        public string IdAttributeAtBeginning { get; set; }
        public bool AtBeginning { get; set; }

        public string IdAttributeCount { get; set; }
        public long Count { get; set; }

        public string IdAttributeOffset { get; set; }
        public long Offset { get; set; }
    }
}
