﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRunModule.Models
{
    public class ReportDataItem
    {
        public List<CommandLineRunToReport> CommandLineRunsToReport { get; set; } = new List<CommandLineRunToReport>();
        public clsOntologyItem reportItem { get; set; }
        public List<Dictionary<string, object>> data { get; set; } = new List<Dictionary<string, object>>();
    }
}
