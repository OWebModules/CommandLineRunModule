﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRunModule.Models
{
    public class VarValue
    {
        public string Variable { get; set; }
        public string Value { get; set; }
        public clsOntologyItem VariableItem { get; set; }
        public clsOntologyItem ValueItem { get; set; }
        public clsOntologyItem RefItem { get; set; }
    }
}
