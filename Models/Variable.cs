﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRunModule.Models
{
    public class Variable
    {
        public string IdVariable { get; set; }
        public string NameVariable { get; set; }
    }
}
