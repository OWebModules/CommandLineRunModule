﻿using ReportModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRunModule.Models
{
    public class VariableToField
    {
        public string IdVariableToField { get; set; }
        public string NameVariableToField { get; set; }

        public List<FieldReplace> FieldReplaces { get; set; }

        public Variable Variable { get; set; }
        
        public ReportField ReportField { get; set; }
    }
}
