﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRunModule.Models
{
    public class VariableValueRequest
    {
        public List<VariableValue> Rows { get; set; } = new List<VariableValue>();
    }

    public class VariableValue
    {
        public string Variable { get; set; }
        public string Value { get; set; }
    }
}
