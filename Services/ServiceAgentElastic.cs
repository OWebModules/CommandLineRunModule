﻿using CommandLineRunModule.Models;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRunModule.Services
{
    public class ServiceAgentElastic
    {
        private Globals globals;

        public async Task<ResultItem<CodeSnippletRaw>> GetCodeSnipplets(clsOntologyItem refItem)
        {
            var taskResult = await Task.Run<ResultItem<CodeSnippletRaw>>(() =>
            {
                var result = new ResultItem<CodeSnippletRaw>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new CodeSnippletRaw()
                };

                var dbReaderRefAndCodeSnipplets = new OntologyModDBConnector(globals);
                var dbReaderCodeSnipplet = new OntologyModDBConnector(globals);

                if (refItem.GUID_Parent != Config.LocalData.Class_Code_Snipplets.GUID)
                {
                    var searchRefItemToCodeSnippletLeftRight = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Object = refItem.GUID,
                            ID_Parent_Other = Config.LocalData.Class_Code_Snipplets.GUID
                        }
                    };

                    if (searchRefItemToCodeSnippletLeftRight.Any())
                    {
                        result.ResultState = dbReaderRefAndCodeSnipplets.GetDataObjectRel(searchRefItemToCodeSnippletLeftRight);
                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }


                    }

                    result.Result.CodeSnipplets = dbReaderRefAndCodeSnipplets.ObjectRels.Select(objRel => new clsOntologyItem
                    {
                        GUID = objRel.ID_Other,
                        Name = objRel.Name_Other,
                        GUID_Parent = objRel.ID_Parent_Other,
                        Type = objRel.Ontology
                    }).ToList();


                    if (!result.Result.CodeSnipplets.Any())
                    {
                        var searchRefItemToCodeSnippletRightLeft = new List<clsObjectRel>
                        {
                            new clsObjectRel
                            {
                                ID_Other = refItem.GUID,
                                ID_Parent_Object = Config.LocalData.Class_Code_Snipplets.GUID
                            }
                        };

                        if (searchRefItemToCodeSnippletRightLeft.Any())
                        {
                            result.ResultState = dbReaderRefAndCodeSnipplets.GetDataObjectRel(searchRefItemToCodeSnippletRightLeft);
                            if (result.ResultState.GUID == globals.LState_Error.GUID)
                            {
                                return result;
                            }


                        }

                        result.Result.CodeSnipplets = dbReaderRefAndCodeSnipplets.ObjectRels.Select(objRel => new clsOntologyItem
                        {
                            GUID = objRel.ID_Object,
                            Name = objRel.Name_Object,
                            GUID_Parent = objRel.ID_Parent_Object,
                            Type = globals.Type_Object
                        }).ToList();
                    }
                }
                else
                {
                    var searchCodeSnipplet = new List<clsOntologyItem>
                    {
                        new clsOntologyItem
                        {
                            GUID = refItem?.GUID,
                            GUID_Parent = Config.LocalData.Class_Code_Snipplets.GUID
                        }
                    };

                    result.ResultState = dbReaderCodeSnipplet.GetDataObjects(searchCodeSnipplet);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.CodeSnipplets = dbReaderCodeSnipplet.Objects1;

                }

                result.Result.RefItemToCodeSnipplets = dbReaderRefAndCodeSnipplets.ObjectRels;








                var searchCode = result.Result.CodeSnipplets.Select(obj => new clsObjectAtt
                {
                    ID_Object = obj.GUID,
                    ID_AttributeType = Config.LocalData.AttributeType_Code.GUID
                }).ToList();

                var dbReaderCode = new OntologyModDBConnector(globals);

                if (searchCode.Any())
                {
                    result.ResultState = dbReaderCode.GetDataObjectAtt(searchCode);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                result.Result.CodeOfCodeSnipplets = dbReaderCode.ObjAtts;

                var dbReaderProgrammingLanguage = new OntologyModDBConnector(globals);

                var searchProgLang = dbReaderCodeSnipplet.Objects1.Select(codeSnipplet => new clsObjectRel
                {
                    ID_Object = codeSnipplet.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Code_Snipplets_is_written_in_Programing_Language.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Code_Snipplets_is_written_in_Programing_Language.ID_Class_Right
                }).ToList();

                if (searchProgLang.Any())
                {
                    result.ResultState = dbReaderProgrammingLanguage.GetDataObjectRel(searchProgLang);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                result.Result.CodeSnippletToProgramingLanguage = dbReaderProgrammingLanguage.ObjectRels;

                var dbReaderSyntaxHighlighting = new OntologyModDBConnector(globals);

                var searchCodeHighlighting = dbReaderProgrammingLanguage.ObjectRels.Select(progL => new clsObjectRel
                {
                    ID_Other = progL.ID_Other,
                    ID_RelationType = Config.LocalData.ClassRel_Syntax_Highlighting__Ace__belongs_to_Programing_Language.ID_RelationType,
                    ID_Parent_Object = Config.LocalData.ClassRel_Syntax_Highlighting__Ace__belongs_to_Programing_Language.ID_Class_Left
                }).ToList();

                if (searchCodeHighlighting.Any())
                {
                    result.ResultState = dbReaderSyntaxHighlighting.GetDataObjectRel(searchCodeHighlighting);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                result.Result.ProgramingLanguageToSyntaxHighlighting = dbReaderSyntaxHighlighting.ObjectRels;

                var dbReaderVariables = new OntologyModDBConnector(globals);

                var searchVariables = result.Result.CodeSnipplets.Select(codeS => new clsObjectRel
                {
                    ID_Object = codeS.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Code_Snipplets_contains_Variable.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Code_Snipplets_contains_Variable.ID_Class_Right
                }).ToList();

                if (searchVariables.Any())
                {
                    result.ResultState = dbReaderVariables.GetDataObjectRel(searchVariables);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                result.Result.CodeSnippletToVariables = dbReaderVariables.ObjectRels;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<CommandLineOrCodeSnippletRaw>> GetCommandLineOrCodeSnipplet(string idCommandLineOrCodeSnipplet)
        {
            var taskResult = await Task.Run<ResultItem<CommandLineOrCodeSnippletRaw>>(() =>
            {
                var result = new ResultItem<CommandLineOrCodeSnippletRaw>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new CommandLineOrCodeSnippletRaw()
                };

                var searchCommandLineOrCodeSnipplet = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = idCommandLineOrCodeSnipplet,
                        GUID_Parent = Config.LocalData.Class_Comand_Line.GUID
                    },
                    new clsOntologyItem
                    {
                        GUID = idCommandLineOrCodeSnipplet,
                        GUID_Parent = Config.LocalData.Class_Code_Snipplets.GUID
                    }

                };

                var dbReaderCommandLineOrCodeSnipplet = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderCommandLineOrCodeSnipplet.GetDataObjects(searchCommandLineOrCodeSnipplet);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting command line or code snipplet!";
                    return result;
                }

                result.Result.CommandLineOrCodeSnipplet = dbReaderCommandLineOrCodeSnipplet.Objects1.FirstOrDefault();

                if (result.Result.CommandLineOrCodeSnipplet != null && result.Result.CommandLineOrCodeSnipplet.GUID_Parent == Config.LocalData.Class_Code_Snipplets.GUID)
                {
                    var searchCode = new List<clsObjectAtt>
                    {
                        new clsObjectAtt
                        {
                            ID_Object = result.Result.CommandLineOrCodeSnipplet.GUID,
                            ID_AttributeType = Config.LocalData.AttributeType_Code.GUID
                        }
                    };

                    var dbReaderCode = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderCode.GetDataObjectAtt(searchCode);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the code of Code-Snipplet!";
                        return result;
                    }

                    result.Result.Code = dbReaderCode.ObjAtts.FirstOrDefault();
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<CommandLineRunRaw>> GetCommandLineRun(List<clsOntologyItem> commandLineRuns)
        {
            var taskResult = await Task.Run<ResultItem<CommandLineRunRaw>>(async () =>
            {
                var result = new ResultItem<CommandLineRunRaw>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new CommandLineRunRaw()
                };

                result.Result.CommandLineRuns = commandLineRuns;

                foreach (var cmdlr in result.Result.CommandLineRuns)
                {
                    cmdlr.Val_Long = cmdlr.Val_Long == null ? 1 : cmdlr.Val_Long;
                }

                var resultSub = GetSubCommandLineRuns(commandLineRuns);

                if (resultSub.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = resultSub.ResultState;
                    return result;
                }

                result.Result.CommandLineRuns.AddRange(resultSub.Result.Select(tuple => tuple.Item2));
                result.Result.CommandLineRunsHierarchy = resultSub.Result.Select(tuple => tuple.Item1).ToList();

                var resultCommandLines = GetCommandLineRunsToCommandLine(result.Result.CommandLineRuns);

                if (resultCommandLines.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = resultCommandLines.ResultState;
                    return result;
                }

                result.Result.CommandLineRunsToCommandLines = resultCommandLines.Result;

                var resultCommandLinesToProgrammingLanguages = GetProgrammingLanguagesOfCommandLine(result.Result.CommandLineRunsToCommandLines);

                if (resultCommandLinesToProgrammingLanguages.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = resultCommandLinesToProgrammingLanguages.ResultState;
                    return result;
                }
                result.Result.CommandLinesToProgrammingLanguage = resultCommandLinesToProgrammingLanguages.Result;


                var resultCommandLineRunsToCodeSnipplets = GetCommandLineRunsToCodeSnipplets(result.Result.CommandLineRuns);

                if (resultCommandLineRunsToCodeSnipplets.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = resultCommandLineRunsToCodeSnipplets.ResultState;
                    return result;
                }

                result.Result.CommandLineRunsToCodeSnipplets = resultCommandLineRunsToCodeSnipplets.Result;

                var searchCode = result.Result.CommandLineRunsToCodeSnipplets.Select(codeSnipp => new clsObjectAtt
                {
                    ID_Object = codeSnipp.ID_Other,
                    ID_AttributeType = Config.LocalData.AttributeType_Code.GUID
                }).ToList();

                var dbReaderCode = new OntologyModDBConnector(globals);

                if (searchCode.Any())
                {
                    result.ResultState = dbReaderCode.GetDataObjectAtt(searchCode);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState = result.ResultState;
                        return result;
                    }
                }

                result.Result.CodeSnippletsToCode = dbReaderCode.ObjAtts;

                var resultCodeSnippletsToProgrammingLanguages = GetCodeSnippletsToProgrammingLanguage(result.Result.CommandLineRunsToCodeSnipplets);

                if (resultCodeSnippletsToProgrammingLanguages.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = resultCodeSnippletsToProgrammingLanguages.ResultState;
                    return result;
                }

                result.Result.CodeSnippletsToProgrammingLanguages = resultCodeSnippletsToProgrammingLanguages.Result;


                var rels = result.Result.CodeSnippletsToProgrammingLanguages;
                rels.AddRange(result.Result.CommandLinesToProgrammingLanguage);

                var resultSyntaxHeighlighting = GetSyntaxHighlightingToProgrammingLanguage(rels);

                if (resultSyntaxHeighlighting.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = resultSyntaxHeighlighting.ResultState;
                    return result;
                }

                result.Result.ProgramingLanguageToSyntaxHighlighting = resultSyntaxHeighlighting.Result;


                rels = result.Result.CodeSnippletsToProgrammingLanguages;
                rels.AddRange(result.Result.CommandLinesToProgrammingLanguage);

                var resultEncodings = GetProgramingLanguagesToEncodings(rels);

                if (resultEncodings.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = resultEncodings.ResultState;
                    return result;
                }

                result.Result.ProgramingLanguageToEncoding = resultEncodings.Result;

                rels = result.Result.CommandLineRunsToCommandLines;
                rels.AddRange(result.Result.CommandLineRunsToCodeSnipplets);

                var resultVariables = GetCommandLineOrCodeSnippletsToVariables(rels);

                if (resultVariables.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = resultVariables.ResultState;
                    return result;
                }


                result.Result.CommandLineAndCodeSnippletsToVariables = resultVariables.Result;

                var relsCommandLineRunsToValues = GetCommandLineRunsToValues(result.Result.CommandLineRuns);

                if (resultVariables.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = resultVariables.ResultState;
                    return result;
                }

                result.Result.CommandLineRunsToValues = relsCommandLineRunsToValues.Result;

                var resultValuesToReferences = GetValuesToReferences(result.Result.CommandLineRunsToValues);

                if (resultValuesToReferences.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = resultValuesToReferences.ResultState;
                    return result;
                }

                result.Result.ValuesToReferences = resultValuesToReferences.Result;

                var searchCommandLineRuns = (from valuesToRef in result.Result.ValuesToReferences.Where(rel => rel.ID_Parent_Other == Config.LocalData.Class_Code_Snipplets.GUID)
                                             join codeSnipplet in result.Result.CommandLineRunsToCodeSnipplets on valuesToRef.ID_Other equals codeSnipplet.ID_Other into codeSnipplets
                                             from codeSnipplet in codeSnipplets.DefaultIfEmpty()
                                             where codeSnipplet == null
                                             select new clsObjectRel
                                             {
                                                 ID_Other = valuesToRef.ID_Other,
                                                 ID_RelationType = Config.LocalData.ClassRel_Comand_Line__Run__contains_Comand_Line__Run_.ID_RelationType,
                                                 ID_Parent_Object = Config.LocalData.ClassRel_Comand_Line__Run__contains_Comand_Line__Run_.ID_Class_Left
                                             }).ToList();

                var dbReaderCommandLineRunsOfValues = new OntologyModDBConnector(globals);

                if (searchCommandLineRuns.Any())
                {
                    result.ResultState = dbReaderCommandLineRunsOfValues.GetDataObjectRel(searchCommandLineRuns);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }


                var resultGetVariablesToValues = GetValuesToVariables(result.Result.CommandLineRunsToValues);

                if (resultGetVariablesToValues.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = resultGetVariablesToValues.ResultState;
                    return result;
                }

                result.Result.ValuesToVariables = resultGetVariablesToValues.Result;

                var commandLineRunsFromValues = dbReaderCommandLineRunsOfValues.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Object,
                    Name = rel.Name_Object,
                    GUID_Parent = rel.ID_Parent_Object,
                    Type = globals.Type_Object
                }).ToList();

                var resultParents = GetRootItems(commandLineRuns);
                if (resultParents.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = resultParents.ResultState;
                    return result;
                }

                result.Result.CommandLineRunsParentsHierarchy.AddRange(resultParents.Result);

                if (commandLineRunsFromValues.Any())
                {
                    var resultCommandLineRuns = await GetCommandLineRun(commandLineRunsFromValues);
                    if (resultCommandLineRuns.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.CommandLineRuns.AddRange(resultCommandLineRuns.Result.CommandLineRuns);
                    result.Result.CommandLineRunsToCommandLines.AddRange(resultCommandLineRuns.Result.CommandLineRunsToCommandLines);
                    result.Result.CommandLinesToProgrammingLanguage.AddRange(resultCommandLineRuns.Result.CommandLinesToProgrammingLanguage);
                    result.Result.CommandLineRunsToCodeSnipplets.AddRange(resultCommandLineRuns.Result.CommandLineRunsToCodeSnipplets);
                    result.Result.CodeSnippletsToCode.AddRange(resultCommandLineRuns.Result.CodeSnippletsToCode);
                    result.Result.CodeSnippletsToProgrammingLanguages.AddRange(resultCommandLineRuns.Result.CodeSnippletsToProgrammingLanguages);
                    result.Result.ProgramingLanguageToEncoding.AddRange(resultCommandLineRuns.Result.ProgramingLanguageToEncoding);
                    result.Result.ProgramingLanguageToSyntaxHighlighting.AddRange(resultCommandLineRuns.Result.ProgramingLanguageToSyntaxHighlighting);
                    result.Result.CommandLineAndCodeSnippletsToVariables.AddRange(resultCommandLineRuns.Result.CommandLineAndCodeSnippletsToVariables);
                    result.Result.CommandLineRunsToValues.AddRange(resultCommandLineRuns.Result.CommandLineRunsToValues);
                    result.Result.ValuesToReferences.AddRange(resultCommandLineRuns.Result.ValuesToReferences);
                    result.Result.ValuesToVariables.AddRange(resultCommandLineRuns.Result.ValuesToVariables);
                    result.Result.CommandLineRunsParentsHierarchy.AddRange(resultCommandLineRuns.Result.CommandLineRunsParentsHierarchy);
                }

                var searchReferences = result.Result.CommandLineRuns.Select(cmdrl => new clsObjectRel
                {
                    ID_Object = cmdrl.GUID,
                    ID_RelationType = CommandLineRunModule.Config.LocalData.ClassRel_Comand_Line__Run__references.ID_RelationType
                }).ToList();

                var dbReaderReferences = new OntologyModDBConnector(globals);

                if (searchReferences.Any())
                {
                    result.ResultState = dbReaderReferences.GetDataObjectRel(searchReferences);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the References!";
                        return result;
                    }

                    result.Result.CommandLineRunsToReferences = dbReaderReferences.ObjectRels;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<TransformNamesModel>> GetTransformNamesModel()
        {
            var taskResult = await Task.Run<ResultItem<TransformNamesModel>>(() =>
           {
               var result = new ResultItem<TransformNamesModel>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new TransformNamesModel {
                       BaseConfig = Config.LocalData.Object_BaseConfig__Template_.Clone()
                   }
               };

               var searchTemplate = new List<clsObjectAtt>
               {
                   new clsObjectAtt
                   {
                       ID_Object = Config.LocalData.Object_BaseConfig__Template_.GUID,
                       ID_AttributeType = Config.LocalData.ClassAtt_ComandlineRun_Transform_Name_Template.ID_AttributeType
                   }
               };

               var dbReaderTemplate = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderTemplate.GetDataObjectAtt(searchTemplate);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Template!";
                   return result;
               }

               result.Result.TransformName = dbReaderTemplate.ObjAtts.FirstOrDefault();

               return result;
           });

            return taskResult;
        }

        public ResultItem<List<clsObjectRel>> GetRootItems(List<clsOntologyItem> commandLineRuns)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var searchParent = commandLineRuns.Select(cmdrl => new clsObjectRel
            {
                ID_Other = cmdrl.GUID,
                ID_RelationType = Config.LocalData.ClassRel_Comand_Line__Run__contains_Comand_Line__Run_.ID_RelationType,
                ID_Parent_Object = Config.LocalData.ClassRel_Comand_Line__Run__contains_Comand_Line__Run_.ID_Class_Left
            }).ToList();

            var dbReaderParent = new OntologyModDBConnector(globals);

            if (searchParent.Any())
            {
                result.ResultState = dbReaderParent.GetDataObjectRel(searchParent);

                result.Result = dbReaderParent.ObjectRels;
            }

            if (result.Result.Any())
            {
                var resultParentItems = GetRootItems(result.Result.Select(rel => new clsOntologyItem { GUID = rel.ID_Object, Name = rel.Name_Object, GUID_Parent = rel.ID_Parent_Object, Type = globals.Type_Object }).ToList());
                if (resultParentItems.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = resultParentItems.ResultState;
                    return result;
                }

                result.Result.AddRange(resultParentItems.Result);
            }

            return result;
        }

        public ResultItem<List<clsObjectRel>> GetProgramingLanguagesToEncodings(List<clsObjectRel> commandLineOrCodeSnippletsToProgramingLanguages)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var searchProgramingLanguagesToEncodings = commandLineOrCodeSnippletsToProgramingLanguages.Select(rel => new clsObjectRel
            {
                ID_Object = rel.ID_Other,
                ID_RelationType = Config.LocalData.ClassRel_Programing_Language_belonging_Destination_Encoding.ID_RelationType,
                ID_Other = Config.LocalData.ClassRel_Programing_Language_belonging_Destination_Encoding.ID_Class_Right
            }).ToList();

            var dbReaderProgramingLanguagesToEncodings = new OntologyModDBConnector(globals);

            if (searchProgramingLanguagesToEncodings.Any())
            {
                result.ResultState = dbReaderProgramingLanguagesToEncodings.GetDataObjectRel(searchProgramingLanguagesToEncodings);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
            }

            result.Result = dbReaderProgramingLanguagesToEncodings.ObjectRels;

            return result;
        }

        public ResultItem<List<clsObjectRel>> GetValuesToVariables(List<clsObjectRel> commandLineRunsToValues)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var searchValuesToVariables = commandLineRunsToValues.Select(cmdlrsToValue => new clsObjectRel
            {
                ID_Object = cmdlrsToValue.ID_Other,
                ID_RelationType = Config.LocalData.ClassRel_Value_belongs_to_Variable.ID_RelationType,
                ID_Parent_Other = Config.LocalData.ClassRel_Value_belongs_to_Variable.ID_Class_Right
            }).ToList();

            var dbReaderValuesToVariables = new OntologyModDBConnector(globals);

            if (searchValuesToVariables.Any())
            {
                result.ResultState = dbReaderValuesToVariables.GetDataObjectRel(searchValuesToVariables);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
            }

            result.Result = dbReaderValuesToVariables.ObjectRels;

            return result;
        }

        public async Task<clsOntologyItem> SaveRelations(List<clsObjectRel> relations)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
           {
               var result = globals.LState_Success.Clone();

               var dbWriter = new OntologyModDBConnector(globals);
               result = dbWriter.SaveObjRel(relations);

               if (result.GUID == globals.LState_Error.GUID)
               {
                   result.Additional1 = "Error while saving the relations!";
                   return result;
               }

               return result;
           });

            return taskResult;
        }

        public ResultItem<List<clsObjectRel>> GetRelationVarValueCmdrl(clsOntologyItem cmdLrItem, clsOntologyItem variableItem, clsOntologyItem valueItem)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var relationConfig = new clsRelationConfig(globals);

            var valToVar = relationConfig.Rel_ObjectRelation(valueItem, variableItem, Config.LocalData.RelationType_belongs_to);
            var cmdrlToVar  = relationConfig.Rel_ObjectRelation(cmdLrItem, valueItem, Config.LocalData.RelationType_needs);

            result.Result.Add(valToVar);
            result.Result.Add(cmdrlToVar);

            return result;   
        }

        public ResultItem<List<clsObjectRel>> GetValuesToReferences(List<clsObjectRel> commandLineRunsToValues = null)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var searchValuesToReferences = new List<clsObjectRel>();

            if (commandLineRunsToValues != null)
            {
                searchValuesToReferences = commandLineRunsToValues.Select(cmdlrsToValue => new clsObjectRel
                {
                    ID_Object = cmdlrsToValue.ID_Other,
                    ID_RelationType = Config.LocalData.RelationType_belonging_Source.GUID
                }).ToList();
            }
            else
            {
                var searchValues = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID_Parent = Config.LocalData.Class_Value.GUID
                    }
                };

                var dbReaderValues = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderValues.GetDataObjects(searchValues);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Values!";
                    return result;
                }

                searchValuesToReferences = dbReaderValues.Objects1.Select(val => new clsObjectRel
                {
                    ID_Object = val.GUID,
                    ID_RelationType = Config.LocalData.RelationType_belonging_Source.GUID
                }).ToList();
            }
            

            var dbReaderValuesToReferences = new OntologyModDBConnector(globals);

            if (searchValuesToReferences.Any())
            {
                result.ResultState = dbReaderValuesToReferences.GetDataObjectRel(searchValuesToReferences);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
            }

            result.Result = dbReaderValuesToReferences.ObjectRels;

            return result;
        }

        public ResultItem<List<clsObjectRel>> GetCommandLineRunsToValues(List<clsOntologyItem> commandLineRuns)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var searchCommandLineRunsToValues = commandLineRuns.Select(cmdLRun => new clsObjectRel
            {
                ID_Object = cmdLRun.GUID,
                ID_RelationType = Config.LocalData.ClassRel_Comand_Line__Run__needs_Value.ID_RelationType,
                ID_Parent_Other = Config.LocalData.ClassRel_Comand_Line__Run__needs_Value.ID_Class_Right
            }).ToList();

            var dbReaderCommandLineRunsToValues = new OntologyModDBConnector(globals);

            if (searchCommandLineRunsToValues.Any())
            {
                result.ResultState = dbReaderCommandLineRunsToValues.GetDataObjectRel(searchCommandLineRunsToValues);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
            }

            result.Result = dbReaderCommandLineRunsToValues.ObjectRels;

            return result;
        }

        public ResultItem<List<clsObjectRel>> GetCommandLineRunsToCommandLine(List<clsOntologyItem> commandLineRun)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var searchCommandLine = commandLineRun.Select(cmdLRun => new clsObjectRel
            {
                ID_Object = cmdLRun.GUID,
                ID_RelationType = Config.LocalData.ClassRel_Comand_Line__Run__belongs_to_Comand_Line.ID_RelationType,
                ID_Parent_Other = Config.LocalData.ClassRel_Comand_Line__Run__belongs_to_Comand_Line.ID_Class_Right
            }).ToList();

            var dbReaderCommands = new OntologyModDBConnector(globals);

            if (searchCommandLine.Any())
            {
                result.ResultState = dbReaderCommands.GetDataObjectRel(searchCommandLine);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
            }

            result.Result = dbReaderCommands.ObjectRels;

            return result;
        }

        public ResultItem<List<clsObjectRel>> GetProgrammingLanguagesOfCommandLine(List<clsObjectRel> commandLineRunsToCommandLines)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var searchCommandLinesToProgrammingLanguages = commandLineRunsToCommandLines.Select(cmd => new clsObjectRel
            {
                ID_Object = cmd.ID_Other,
                ID_RelationType = Config.LocalData.ClassRel_Comand_Line_belongs_to_Programing_Language.ID_RelationType,
                ID_Parent_Other = Config.LocalData.ClassRel_Comand_Line_belongs_to_Programing_Language.ID_Class_Right
            }).ToList();

            var dbReaderCommandLinesToProgrammingLanguages = new OntologyModDBConnector(globals);

            if (searchCommandLinesToProgrammingLanguages.Any())
            {
                result.ResultState = dbReaderCommandLinesToProgrammingLanguages.GetDataObjectRel(searchCommandLinesToProgrammingLanguages);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
            }

            result.Result = dbReaderCommandLinesToProgrammingLanguages.ObjectRels;

            return result;
        }

        public ResultItem<List<clsObjectRel>> GetCommandLineRunsToCodeSnipplets(List<clsOntologyItem> commandlineRuns)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var searchCommandLineRunToCodeSnipplets = commandlineRuns.Select(cmdrl => new clsObjectRel
            {
                ID_Object = cmdrl.GUID,
                ID_RelationType = Config.LocalData.ClassRel_Comand_Line__Run__belongs_to_Code_Snipplets.ID_RelationType,
                ID_Parent_Other = Config.LocalData.ClassRel_Comand_Line__Run__belongs_to_Code_Snipplets.ID_Class_Right
            }).ToList();

            var dbReaderCommandLineRunsToCodeSnipplets = new OntologyModDBConnector(globals);

            if (searchCommandLineRunToCodeSnipplets.Any())
            {
                result.ResultState = dbReaderCommandLineRunsToCodeSnipplets.GetDataObjectRel(searchCommandLineRunToCodeSnipplets);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
            }

            result.Result = dbReaderCommandLineRunsToCodeSnipplets.ObjectRels;

            return result;
        }

        public ResultItem<List<Tuple<clsObjectRel, clsOntologyItem>>> GetSubCommandLineRuns(List<clsOntologyItem> commandLineRuns)
        {

            var result = new ResultItem<List<Tuple<clsObjectRel, clsOntologyItem>>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<Tuple<clsObjectRel, clsOntologyItem>>()
            };

            var searchSubItems = commandLineRuns.Select(cmdlr => new clsObjectRel
            {
                ID_Object = cmdlr.GUID,
                ID_RelationType = Config.LocalData.ClassRel_Comand_Line__Run__contains_Comand_Line__Run_.ID_RelationType,
                ID_Parent_Other = Config.LocalData.ClassRel_Comand_Line__Run__contains_Comand_Line__Run_.ID_Class_Right
            }
            ).ToList();

            var dbReaderSubCmdrl = new OntologyModDBConnector(globals);

            result.ResultState = dbReaderSubCmdrl.GetDataObjectRel(searchSubItems);

            result.Result = dbReaderSubCmdrl.ObjectRels.Select(rel => new Tuple<clsObjectRel, clsOntologyItem>(
                rel,
                new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology,
                    Val_Long = rel.OrderID.Value + 1
                })).ToList();

            var additionalItems = new List<Tuple<clsObjectRel, clsOntologyItem>>();

            foreach (var cmdrl in result.Result)
            {
                var resultSub = GetSubCommandLineRuns(new List<clsOntologyItem> { cmdrl.Item2 });
                if (resultSub.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = resultSub.ResultState;
                    break;

                }

                additionalItems.AddRange(resultSub.Result);
            }

            result.Result.AddRange(additionalItems);

            return result;
        }

        public ResultItem<List<clsObjectRel>> GetCodeSnippletsToProgrammingLanguage(List<clsObjectRel> commandlineRunsToCodeSnipplets)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var searchCodeSnippletProgramminglanguages = commandlineRunsToCodeSnipplets.Select(codeSnipp => new clsObjectRel
            {
                ID_Object = codeSnipp.ID_Other,
                ID_RelationType = Config.LocalData.ClassRel_Code_Snipplets_is_written_in_Programing_Language.ID_RelationType,
                ID_Parent_Other = Config.LocalData.ClassRel_Code_Snipplets_is_written_in_Programing_Language.ID_Class_Right
            }).ToList();

            var dbReaderCodeSnippletsToProgrammingLanguages = new OntologyModDBConnector(globals);

            if (searchCodeSnippletProgramminglanguages.Any())
            {
                result.ResultState = dbReaderCodeSnippletsToProgrammingLanguages.GetDataObjectRel(searchCodeSnippletProgramminglanguages);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
            }

            result.Result = dbReaderCodeSnippletsToProgrammingLanguages.ObjectRels;

            return result;
        }

        public ResultItem<List<clsObjectRel>> GetSyntaxHighlightingToProgrammingLanguage(List<clsObjectRel> commandLineOrCodeSnippletsToProgrammingLanguages)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var searchCodeHighlighting = commandLineOrCodeSnippletsToProgrammingLanguages.Select(progL => new clsObjectRel
            {
                ID_Other = progL.ID_Other,
                ID_RelationType = Config.LocalData.ClassRel_Syntax_Highlighting__Ace__belongs_to_Programing_Language.ID_RelationType,
                ID_Parent_Object = Config.LocalData.ClassRel_Syntax_Highlighting__Ace__belongs_to_Programing_Language.ID_Class_Left
            }).ToList();

            var dbReaderSyntaxHighlightingToProgrammingLanguages = new OntologyModDBConnector(globals);

            if (searchCodeHighlighting.Any())
            {
                result.ResultState = dbReaderSyntaxHighlightingToProgrammingLanguages.GetDataObjectRel(searchCodeHighlighting);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
            }

            result.Result = dbReaderSyntaxHighlightingToProgrammingLanguages.ObjectRels;

            return result;
        }

        public ResultItem<List<clsObjectRel>> GetCommandLineOrCodeSnippletsToVariables(List<clsObjectRel> commandLineRunToCommandLineOrCodeSnipplets)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var searchCommandLineToVariables = commandLineRunToCommandLineOrCodeSnipplets.Select(cmd => new clsObjectRel
            {
                ID_Object = cmd.ID_Other,
                ID_RelationType = Config.LocalData.RelationType_contains.GUID,
                ID_Parent_Other = Config.LocalData.Class_Variable.GUID
            }).ToList();

            var dbReaderCommandLineAndCodeSnippletsToVariables = new OntologyModDBConnector(globals);

            if (searchCommandLineToVariables.Any())
            {
                result.ResultState = dbReaderCommandLineAndCodeSnippletsToVariables.GetDataObjectRel(searchCommandLineToVariables);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
            }
            result.Result = dbReaderCommandLineAndCodeSnippletsToVariables.ObjectRels;

            return result;
        }

        public ResultItem<List<clsObjectRel>> GetCommandLineOrCodeSnippletsToVariables(string idCommandLineOrCodeSnipplet)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var searchCommandLineToVariables = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = idCommandLineOrCodeSnipplet,
                    ID_RelationType = Config.LocalData.RelationType_contains.GUID,
                    ID_Parent_Other = Config.LocalData.Class_Variable.GUID
                }
            };

            var dbReaderCommandLineAndCodeSnippletsToVariables = new OntologyModDBConnector(globals);

            if (searchCommandLineToVariables.Any())
            {
                result.ResultState = dbReaderCommandLineAndCodeSnippletsToVariables.GetDataObjectRel(searchCommandLineToVariables);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
            }
            result.Result = dbReaderCommandLineAndCodeSnippletsToVariables.ObjectRels;

            return result;
        }

        public async Task<ResultOItem<ClassObject>> GetOItem(string idObject)
        {
            var taskResult = await Task.Run<ResultOItem<ClassObject>>(() =>
            {
                var result = new ResultOItem<ClassObject>()
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new ClassObject()
                };

                var dbReader = new OntologyModDBConnector(globals);

                var objectItem = dbReader.GetOItem(idObject, globals.Type_Object);

                if (objectItem.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    return result;
                }

                result.Result.ObjectItem = objectItem;

                var classItem = dbReader.GetOItem(objectItem.GUID_Parent, globals.Type_Class);

                if (classItem.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    return result;
                }

                result.Result.ClassItem = classItem;

                return result;
            });
            return taskResult;
        }


        public async Task<ResultItem<CommandLineRunToReportRaw>> GetCommandLineRunToReport(clsOntologyItem reportItem)
        {
            var taskResult = await Task.Run<ResultItem<CommandLineRunToReportRaw>>(() =>
            {
                var result = new ResultItem<CommandLineRunToReportRaw>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new CommandLineRunToReportRaw
                    {
                        ReportItem = reportItem
                    }
                };

                var searchReportToCommandLineRun = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Other = reportItem.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_Comand_Line__Run__To_Report_belongs_to_Reports.ID_RelationType,
                        ID_Parent_Object = Config.LocalData.ClassRel_Comand_Line__Run__To_Report_belongs_to_Reports.ID_Class_Left
                    }
                };

                var dbReaderReportToCommandLineRun = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderReportToCommandLineRun.GetDataObjectRel(searchReportToCommandLineRun);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.ReportItemToCommandLineRunToReport = dbReaderReportToCommandLineRun.ObjectRels;

                var searchReportToCommandLineRunToCommandLineRun = dbReaderReportToCommandLineRun.ObjectRels.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Object,
                    ID_RelationType = Config.LocalData.ClassRel_Comand_Line__Run__To_Report_belongs_to_Comand_Line__Run_.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Comand_Line__Run__To_Report_belongs_to_Comand_Line__Run_.ID_Class_Right
                }).ToList();

                var dbReaderReportToCommandLineRunToCommandLineRun = new OntologyModDBConnector(globals);

                if (searchReportToCommandLineRunToCommandLineRun.Any())
                {
                    result.ResultState = dbReaderReportToCommandLineRunToCommandLineRun.GetDataObjectRel(searchReportToCommandLineRunToCommandLineRun);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                result.Result.CmdrlToRepToCommandLineRun = dbReaderReportToCommandLineRunToCommandLineRun.ObjectRels;

                var searchRepToCmdrlToRemoveCharacter = dbReaderReportToCommandLineRun.ObjectRels.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Object,
                    ID_RelationType = Config.LocalData.ClassRel_Comand_Line__Run__To_Report_uses_Remove_Characters.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Comand_Line__Run__To_Report_uses_Remove_Characters.ID_Class_Right
                }).ToList();

                var dbReaderRepToCmdrlToRemoveCharacter = new OntologyModDBConnector(globals);

                if (searchRepToCmdrlToRemoveCharacter.Any())
                {
                    result.ResultState = dbReaderRepToCmdrlToRemoveCharacter.GetDataObjectRel(searchRepToCmdrlToRemoveCharacter);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                result.Result.CmdrlToRepToRemoveCharacter = dbReaderRepToCmdrlToRemoveCharacter.ObjectRels;

                var searchRemoveCharacterAttributes = result.Result.CmdrlToRepToRemoveCharacter.Select(rel => new clsObjectAtt
                {
                    ID_Object = rel.ID_Other,
                    ID_AttributeType = Config.LocalData.AttributeType_At_Begin.GUID
                }).ToList();

                searchRemoveCharacterAttributes.AddRange(result.Result.CmdrlToRepToRemoveCharacter.Select(rel => new clsObjectAtt
                {
                    ID_Object = rel.ID_Other,
                    ID_AttributeType = Config.LocalData.AttributeType_Count.GUID
                }));

                searchRemoveCharacterAttributes.AddRange(result.Result.CmdrlToRepToRemoveCharacter.Select(rel => new clsObjectAtt
                {
                    ID_Object = rel.ID_Other,
                    ID_AttributeType = Config.LocalData.AttributeType_Offset.GUID
                }));

                var dbReaderRemoveCharacterAttributes = new OntologyModDBConnector(globals);

                if (searchRemoveCharacterAttributes.Any())
                {
                    result.ResultState = dbReaderRemoveCharacterAttributes.GetDataObjectAtt(searchRemoveCharacterAttributes);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                result.Result.CmdrlToRepToRemoveCharacterAttributes = dbReaderRemoveCharacterAttributes.ObjAtts;

                var searchVariablesToField = result.Result.ReportItemToCommandLineRunToReport.Select(rel => new clsObjectRel
                {
                    ID_Other = rel.ID_Object,
                    ID_RelationType = Config.LocalData.ClassRel_Variable_To_Field_belongs_to_Comand_Line__Run__To_Report.ID_RelationType,
                    ID_Parent_Object = Config.LocalData.ClassRel_Variable_To_Field_belongs_to_Comand_Line__Run__To_Report.ID_Class_Left
                }).ToList();

                var dbReaderVariablesToFields = new OntologyModDBConnector(globals);

                if (searchVariablesToField.Any())
                {
                    result.ResultState = dbReaderVariablesToFields.GetDataObjectRel(searchVariablesToField);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the variables to fields!";
                        return result;
                    }
                }

                result.Result.CmdrlToRepToVariablesToFields = dbReaderVariablesToFields.ObjectRels;

                var searchReportFieldsAndVariables = result.Result.CmdrlToRepToVariablesToFields.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Object,
                    ID_RelationType = Config.LocalData.ClassRel_Variable_To_Field_belongs_to_Report_Field.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Variable_To_Field_belongs_to_Report_Field.ID_Class_Right
                }).ToList();

                searchReportFieldsAndVariables.AddRange(result.Result.CmdrlToRepToVariablesToFields.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Object,
                    ID_RelationType = Config.LocalData.ClassRel_Variable_To_Field_belongs_to_Variable.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Variable_To_Field_belongs_to_Variable.ID_Class_Right
                }));

                var dbReaderFieldsAndVariables = new OntologyModDBConnector(globals);

                if (searchReportFieldsAndVariables.Any())
                {
                    result.ResultState = dbReaderFieldsAndVariables.GetDataObjectRel(searchReportFieldsAndVariables);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Report-Fields and Variables!";
                        return result;
                    }
                }

                result.Result.VarToFieldToReportFields = dbReaderFieldsAndVariables.ObjectRels.Where(rel => rel.ID_Parent_Other == Config.LocalData.ClassRel_Variable_To_Field_belongs_to_Report_Field.ID_Class_Right).ToList();
                result.Result.VarToFieldToVariables = dbReaderFieldsAndVariables.ObjectRels.Where(rel => rel.ID_Parent_Other == Config.LocalData.ClassRel_Variable_To_Field_belongs_to_Variable.ID_Class_Right).ToList();



                return result;
            });


            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> SaveObjects(List<clsOntologyItem> objectList)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };

                objectList.Where(obj => string.IsNullOrEmpty( obj.GUID)).ToList().ForEach(obj =>
                {
                    obj.GUID = globals.NewGUID;
                });

                if (objectList.Any())
                {
                    var dbWriterVariable = new OntologyModDBConnector(globals);

                    result.ResultState = dbWriterVariable.SaveObjects(objectList);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving objects!";
                        return result;
                    }
                }

                result.Result.AddRange(objectList);

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> CheckObjects(List<clsOntologyItem> objectList)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };

                var searchVariables = objectList.Where(var => string.IsNullOrEmpty(var.GUID)).ToList();

                if (searchVariables.Any())
                {
                    var dbReaderVariables = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderVariables.GetDataObjects(searchVariables);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while searching objects!";
                        return result;
                    }

                    result.Result = (from variable in objectList
                                     join dbVariable in dbReaderVariables.Objects1 on variable.Name equals dbVariable.Name
                                     select dbVariable).ToList();
                }

                var varsToSave = (from variable in objectList
                                  join existing in result.Result on variable.Name equals existing.Name into dbVariables
                                  from existing in dbVariables.DefaultIfEmpty()
                                  where existing == null
                                  select variable).ToList();

                varsToSave.ForEach(var =>
                {
                    var.GUID = globals.NewGUID;
                });

                if (varsToSave.Any())
                {
                    var dbWriterVariable = new OntologyModDBConnector(globals);

                    result.ResultState = dbWriterVariable.SaveObjects(varsToSave);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving objects!";
                        return result;
                    }
                }

                result.Result.AddRange(varsToSave);

                result.Result.AddRange(objectList.Where(var1 => !string.IsNullOrEmpty(var1.GUID)));

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveCommandLineOrCodeSnippletToVariable(clsOntologyItem commandLineOrCodeSnipplet, List<clsOntologyItem> variables)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var relationConfig = new clsRelationConfig(globals);

                var varOrderId = 1;
                var searchRelations = variables.Select(var => relationConfig.Rel_ObjectRelation(commandLineOrCodeSnipplet, var, Config.LocalData.RelationType_contains, orderId: varOrderId++)).ToList();

                var dbConnector = new OntologyModDBConnector(globals);

                result = dbConnector.GetDataObjectRel(searchRelations);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while searching relations between Command Line or Code Snipplet and Variables!";
                    return result;
                }

                var relsToSave = (from rel in searchRelations
                                  join dbRel in dbConnector.ObjectRels on new { rel.ID_Object, rel.ID_Other, rel.ID_RelationType } equals new { dbRel.ID_Object, dbRel.ID_Other, dbRel.ID_RelationType } into dbRels
                                  from dbRel in dbRels.DefaultIfEmpty()
                                  where dbRel == null
                                  select rel).ToList();

                if (relsToSave.Any())
                {
                    result = dbConnector.SaveObjRel(relsToSave);

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving relations between Command Line or Code Snipplet and Variables!";
                        return result;
                    }
                }
                

                return result;
            });

            return taskResult;
        }

        public ServiceAgentElastic(Globals globals)
        {
            this.globals = globals;
        }
    }

    

    public class CodeSnippletRaw
    {
        public List<clsOntologyItem> CodeSnipplets { get; set; }
        public List<clsObjectRel> RefItemToCodeSnipplets { get; set; }
        public List<clsObjectAtt> CodeOfCodeSnipplets { get; set; }
        public List<clsObjectRel> CodeSnippletToProgramingLanguage { get; set; }
        public List<clsObjectRel> ProgramingLanguageToSyntaxHighlighting { get; set; }
        public List<clsObjectRel> CodeSnippletToVariables { get; set; }
    }
}
