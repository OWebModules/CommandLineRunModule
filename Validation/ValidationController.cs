﻿using CommandLineRunModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRunModule.Validation
{
    public static class ValidationController
    {
        public static clsOntologyItem ValidateGetCommandRunsToReportRequest(GetCommandRunsToReportRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (request.ReportItem == null)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The requested Report-Item is empty!";
                return result;
            }

            return result;
        }
    }
}
